import "./chatbot.css";

const animatedNode = document.getElementsByClassName("fadeInAnimated");

Array.from(animatedNode).forEach(function (item, i) {
  item.style.animationDelay = `${i * 2}s`;
  setTimeout(function () {
    item.classList.add("completed");
  }, (i + 1) * 2000);
});
