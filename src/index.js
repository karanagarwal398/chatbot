import "./assets/chat-bot.png";
import "./styles.css";

const frame = document.getElementById("chatbot-frame");
const chatBtn = document.getElementById("chatbot-btn");
const closeBtn = document.getElementById("close");

init();

function init() {
  chatBtn.addEventListener("click", function (event) {
    event.preventDefault();
    openChatBot();
  });

  closeBtn.addEventListener("click", function (event) {
    closeChatBot();
  });

  closeChatBot();
}

function openChatBot() {
  frame.style.display = "block";
  closeBtn.style.display = "block";
  chatBtn.style.display = "none";
  frame.setAttribute("src", "chatbot.html");
}

function closeChatBot() {
  frame.setAttribute("src", "");
  frame.style.display = "none";
  closeBtn.style.display = "none";
  chatBtn.style.display = "block";
}
